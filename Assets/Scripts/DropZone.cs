﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DropZone : MonoBehaviour , IDropHandler{
	///public int CanHold = 0;
	public	void OnDrop(PointerEventData EventData){
		
		GameObject Card = Instantiate(EventData.pointerDrag.gameObject,this.transform);
		Card.GetComponent<CanvasGroup>().blocksRaycasts = true;
		
	}
}
