﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
	/* 
		this is just the first release from the game controller there is another version but i kept this 
		to declare my progress and trails in the game :D 
	 */
	public GameObject Hero;
	Rigidbody HeroRigid;
	Animator HeroAnim;
	public GameObject Field;
	
	private ArrayList Commands = new ArrayList();
    public Vector3 movement = new Vector3(0,0,0);
	private int turn = 0;
	private bool jump = false;
	public bool grounded = true;
	// Use this for initialization
	void Start () {
		 // Debug.Log("Hii");
		  HeroRigid = Hero.GetComponent<Rigidbody>();
		  HeroAnim = Hero.GetComponent<Animator>();
			
	}
	
	// Update is called once per frame
	public void Parse() {
		//Debug.Log("parse");
		GameObject.Find("Start").SetActive(false);
		Commands.Clear();
		for(int i = 0 ; i < Field.transform.childCount ; i++){
			Commands.Add(Field.transform.GetChild(i).GetComponent<Dragabble>().value);
		}
		StartCoroutine(Execute());
	}
	public void Stop(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	void Update(){
		HeroAnim.SetFloat( "walk" , Mathf.Abs(movement.x + movement.z) * Time.deltaTime);
    	HeroAnim.SetFloat( "jump" , Mathf.Abs(movement.y * Time.deltaTime));
	}
	void FixedUpdate(){
		//Debug.Log(movement.y);
		HeroRigid.velocity = movement * Time.deltaTime;		
	}
	Vector3 BuildMovement(int turn , float power , bool jump){
		int absTurn = Mathf.Abs(turn) % 4;
		float Jump = jump ? 50f : 0;
		float absPower = Mathf.Abs(power);
		switch (absTurn) {
			case 0 : return new Vector3(0 , Jump , absPower);
			case 1 : return new Vector3(absPower , Jump , 0);
			case 2 : return new Vector3(0,Jump,-absPower);
			case 3 : return new Vector3(-absPower,Jump,0);
		}
		return new Vector3(0,0,0);
	}
	IEnumerator Execute(){
		float seconds = 0.45f; //* Time.deltaTime;
		foreach(string s in Commands){
			//while(!grounded){
			//	Debug.Log("I Got Here");
			//	yield return new WaitForSeconds(.2f);
			//}
			HeroRigid.mass = 2;
			//Debug.Log(s);
			switch (s) {
				case "walk" :

					movement = BuildMovement(turn , 210f , jump);
						yield return new WaitForSeconds(seconds);
					movement = new Vector3(0 , 0 , 0);	
				break;
				case "jump" : 	
					jump = true;
					grounded = false;
					movement = BuildMovement(turn , 0f , jump);
						yield return new WaitForSeconds(seconds);
					movement = BuildMovement(turn , 200f , jump);
						yield return new WaitForSeconds(seconds);	
					//HeroRigid.velocity=new Vector3(0,-3,0);
					jump = false;
					movement = new Vector3(0 , -60 , 0);
						yield return new WaitForSeconds(seconds/2f);
					movement = new Vector3(0 , 0 , 0);
				break;
				case "turnLeft" :
					Hero.transform.eulerAngles = new Vector3(0, Hero.transform.eulerAngles.y + 90 ,0);
					turn++;
				break ;
				case "turnRight" :
					Hero.transform.eulerAngles = new Vector3(0, Hero.transform.eulerAngles.y - 90 ,0);
					turn--;
				break ;
			}
			yield return new WaitForSeconds(1.2f);
		}
	}



	

	
}
