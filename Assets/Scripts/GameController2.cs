﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
public class GameController2 : MonoBehaviour {
	/*
		This class represents the enhanced version of parser module 
		# All Movement Functions Moved to HeroAnimation script which will be on the cat model (the hero :D)
		# optimized parsing 
		# i kept the previous buggy scripts to declare my progress and trails in the game :D 
	*/
	public HeroAnimation Hero;
	public GameObject Field;
	public GameObject Field2;
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	public void Stop(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	public void parse(){
		GameObject.Find("Start").SetActive(false);
		for(int i = 0 ; i < Field.transform.childCount ; i++){
	
			string command = Field.transform.GetChild(i).GetComponent<Dragabble>().value;
			try{
				Excute(command,i,Field);
			}
			catch(Exception e){
				Debug.Log(e.Message);
				Stop();
			}
		}
	}
	void Excute(string command,int order, GameObject Holder){
		string looppattern = @"loop";
		Match loopresult = Regex.Match(command, looppattern);
		if (loopresult.Success)
		{
			if(order != 0){
				string previouscommand = Holder.transform.GetChild(order-1).GetComponent<Dragabble>().value;
				for(int j = 0 ; j < Int32.Parse(command.Replace("loop",""))-1 ; j++){
					Excute(previouscommand,order-1,Holder);
				}
			}
		}
	    string routinepattern = @"routine";
	    Match routineresult = Regex.Match(command, routinepattern);
		if (routineresult.Success)
		{
			// this case for routine of routine (unity freeze in this case without this solution)
			// so when it come to recursion i replace it with loop and all levels won't need more levels deep than 5	
			if(Holder.name == "Field2"){ 
				for(int k = 0 ; k < 5 ; k++){
					for(int z = 0 ; z < order-1 ; z++){
						string c = Holder.transform.GetChild(z).GetComponent<Dragabble>().value;
						Excute(c,z,Holder);
					}
				}
			}
			else{
				for(int i = 0 ; i < Field2.transform.childCount ; i++){
					string command2 = Field2.transform.GetChild(i).GetComponent<Dragabble>().value;
					Excute(command2,i,Field2);
				}
			}
		}
		switch (command) {
				case "walk" :
					Hero.Move();
				break;
				case "jump" : 	
					Hero.Jump();
				break;
				case "turnLeft" :
					Hero.TurnLeft();
				break ;
				case "turnRight" :
					Hero.TurnRight();
				break ;
			}
	}
	
	
}
