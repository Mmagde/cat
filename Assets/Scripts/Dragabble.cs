﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
// https://www.youtube.com/watch?v=bMuYUOIAdnc&list=PLbghT7MmckI42Gkp2cILkO2nRxK2M4NLo Source learned drag and drop from
public class Dragabble : MonoBehaviour , IBeginDragHandler , IDragHandler , IEndDragHandler {
	public Transform ParentTransform;
	public string value = null;
	public void OnBeginDrag(PointerEventData EventData){
		ParentTransform = this.transform.parent; 
		//Debug.Log("Begin");
		this.transform.SetParent(this.transform.parent.parent);
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	public void OnDrag(PointerEventData EventData){
		//Debug.Log("Drag");
		this.transform.position = EventData.position;
	}
	public void OnEndDrag(PointerEventData EventData){
		this.transform.SetParent (ParentTransform) ; 
		//Debug.Log("End");
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
}
