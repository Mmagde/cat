﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAnimation : MonoBehaviour {

	public Vector3 movement = new Vector3(0,0,0);
	public Vector3 anim = new Vector3(0,0,0);
	Rigidbody HeroRigid;
	Animator HeroAnim;
	bool locked = false;
	// Use this for initialization
	void Start () {
		 HeroRigid = this.GetComponent<Rigidbody>();
		 HeroAnim = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void FixedUpdate()
	{
		HeroAnim.SetFloat( "walk" , anim.z);
		HeroAnim.SetFloat( "jump" , anim.y);
		HeroRigid.velocity = movement;
	}
	IEnumerator MoveRoutine(float factor){
		while(locked) yield return new WaitForSeconds(.5f);
		locked = true;	
		anim.z = 2;
		movement  =  this.transform.TransformDirection(new Vector3(0,0,1))* factor * Time.deltaTime;
		yield return new WaitForSeconds(2f);	
		movement = Vector3.zero;		
		anim.z = 0;
		locked = false;
	}
	public void Move(float factor = 30f){
		Debug.Log("Move");
		StartCoroutine(MoveRoutine(factor));
		//while(locked);
		Debug.Log("End Move");
	}
	IEnumerator JumpRoutine(float factor){
		while(locked) yield return new WaitForSeconds(.5f);
		locked = true;	
		anim.y = 2;
		movement  =  this.transform.TransformDirection(new Vector3(0,1,1))* 30f * Time.deltaTime;
		yield return new WaitForSeconds(1f);		
		movement  =  this.transform.TransformDirection(new Vector3(0,-1,1))* 30f * Time.deltaTime;
		yield return new WaitForSeconds(1f);
		movement = Vector3.zero;
		anim.y = 0;	
		locked = false;		
	}
	public void Jump(float factor = 30f){
		Debug.Log("Jump");
		StartCoroutine(JumpRoutine(factor));
		Debug.Log("End Jump");
	}
	IEnumerator TurnRightRoutine(){
		while(locked) yield return new WaitForSeconds(.5f);
		locked = true;	
		this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y + 90 ,0);
		locked = false;		
	}
	public void TurnRight(){
		Debug.Log("TurnRight");
		StartCoroutine(TurnRightRoutine());
		//this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y + 90 ,0);
		Debug.Log("TurnRight");
	}
	IEnumerator TurnLeftRoutine(){
		while(locked) yield return new WaitForSeconds(.5f);
		locked = true;	
		this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y - 90 ,0);
		locked = false;		
	}
	public void TurnLeft(){
		Debug.Log("TurnLeft");
		StartCoroutine(TurnLeftRoutine());
		//this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y - 90 ,0);
		Debug.Log("TurnLeft");
	}
}
